from .base import *

DEBUG = True

redis_host = os.environ.get('REDIS_HOST', 'localhost')

# Channel layer definitions
# http://channels.readthedocs.org/en/latest/deploying.html#setting-up-a-channel-backend
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "asgi_redis.RedisChannelLayer",
        "CONFIG": {
            "hosts": [(redis_host, 6379)],
        },
        "ROUTING": "project.routing.channel_routing",
    },
}

ALLOWED_HOSTS = ['*']

AUTH_PASSWORD_VALIDATORS = [

]

WSGI_APPLICATION = 'project.wsgi.local.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'd5ijuev0re4uqj',
        'USER': 'pirbiomvtguaca',
        'PASSWORD': 'c2cf75cb6561a9d0eeb2911506d5f92ce33d9abdabbe9d0ba091fd17d0f97871',
        'HOST': 'ec2-174-129-227-128.compute-1.amazonaws.com',
        'PORT': '5432',
    }
}
